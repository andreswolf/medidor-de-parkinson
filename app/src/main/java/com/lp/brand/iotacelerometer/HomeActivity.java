package com.lp.brand.iotacelerometer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.lp.brand.iotacelerometer.databinding.ActivityHomeBinding;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity  implements SensorEventListener {

    private long last_upadte = 0, last_movement = 0; //Tiempo de ultima actualizacion y tiempo de ultimo movimiento registrado
    private float prevX = 0, prevY = 0, prevZ = 0; //Valores del ultimo movimiento registrado, se utilizan como referencia de un cambio
    private float curX = 0, curY = 0, curZ = 0; //Valores de lectura en tiempo real
    float sensibility, refresh_time; //Vble de sensibilidad y periodo de lectura
    private SensorManager sensorManager;
    private Sensor sensorAccelerometer;
    private ArrayList<Float> datosx,datosy,datosz; //Arreglo de valores de lectura enviados a la actividad MainActivity para graficar
    ImageButton btnstart;
    TextView txttemp;
    Boolean blockback=false;
    SharedPreferences preferences; //Configuracion de la aplicacion
    TextView txtejex, txtejey, txtejez;

    public static float round(float value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        /*long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);*/
        DecimalFormat format = new DecimalFormat("######.#");
        return Float.valueOf(format.format(value));
    }

    public static float round(float value) {
        DecimalFormat format = new DecimalFormat("######,#");
        return Float.valueOf(format.format(value));
    }

    public static float myRound(float value){
        String number = (value+"").split("\\.")[0];
        String decimals = "0";
        try{
            decimals = (value+"").split("\\.")[1];
            decimals = decimals.substring(0,1);
        }catch (Exception e){}
        return Float.parseFloat(number + "." + decimals);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        btnstart = (ImageButton) findViewById(R.id.btnstart);
        txttemp = (TextView) findViewById(R.id.txttemp);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        datosx = new ArrayList<>();
        datosy = new ArrayList<>();
        datosz = new ArrayList<>();
        try{
            sensibility = Float.parseFloat(preferences.getString("edtsettings_sensibility","1E-7f"));
            refresh_time = Float.parseFloat(preferences.getString("edtsettings_refreshtime","0.5E+9f"));
        }catch (NumberFormatException e){
            Intent i = new Intent().setClass(this, Settings.class);
            startActivity(i);
        }
        Log.v("Brandon-lp","sensibilidad ->"+sensibility+" Periodo ->"+refresh_time);
        btnstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int timerecord =Integer.parseInt(preferences.getString("edtsettings_starttime","10"));
                startTimer(timerecord*1000,1000);
            }
        });
        txtejex = (TextView) findViewById(R.id.txtejex);
        txtejey = (TextView) findViewById(R.id.txtejey);
        txtejez = (TextView) findViewById(R.id.txtejez);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensorAccelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    //Hilo de cuenta regresiva de tiempo de lectura
    public void startTimer(final long finish, long tick)
    {   blockback=true;
        btnstart.setEnabled(false);
        datosx.add(curX);
        datosy.add(curY);
        datosz.add(curZ);
        CountDownTimer t = new CountDownTimer(finish, tick) {


            public void onTick(long millisUntilFinished)
            {
                long remainedSecs = millisUntilFinished/1000;
                txttemp.setText(""+(remainedSecs/60)+":"+(remainedSecs%60));
                Log.i("startTimer", "onTick: " + remainedSecs + "");
                datosx.add(curX);
                datosy.add(curY);
                datosz.add(curZ);
            }

            public void onFinish()
            {
                txttemp.setText("00:00:00");
                Intent i = new Intent().setClass(HomeActivity.this, MainActivity.class);
                ArrayList<Float> datosxEstandarizados = new ArrayList<>();
                ArrayList<Float> datosyEstandarizados = new ArrayList<>();
                ArrayList<Float> datoszEstandarizados = new ArrayList<>();
                ArrayList<Float> datosxyzEstandarizados = new ArrayList<>();
                ArrayList<Float> datosxyzEstandarizados2 = new ArrayList<>();
                datosxEstandarizados.add(0.0f);
                datosyEstandarizados.add(0.0f);
                datoszEstandarizados.add(0.0f);
                datosxyzEstandarizados.add(0.0f);
                datosxyzEstandarizados2.add(0.0f);
                for (int j = 1; j < datosx.size(); j++){
                    datosxEstandarizados.add(datosx.get(j) - datosx.get(0));
                    datosyEstandarizados.add(datosy.get(j) - datosy.get(0));
                    datoszEstandarizados.add(datosz.get(j) - datosz.get(0));
                    float exp = ((float)Math.pow(-1,j));
                    //float exp = 1;
                    Log.i("PAJIMETRO", "onFinish exp: " + exp);
                    datosxyzEstandarizados.add(  Math.abs(datosx.get(j) - datosx.get(0)) + Math.abs(datosy.get(j) - datosy.get(0)) + Math.abs(datosz.get(j) - datosz.get(0)));
                    datosxyzEstandarizados2.add( exp * datosxyzEstandarizados.get(j));
                }
                i.putExtra("datosx",datosxEstandarizados);
                i.putExtra("datosy",datosyEstandarizados);
                i.putExtra("datosz",datoszEstandarizados);
                i.putExtra("datosxyz",datosxyzEstandarizados);
                i.putExtra("datosxyz2",datosxyzEstandarizados2);
                startActivity(i);
                cancel();
                finish();
            }
        }.start();
    }
    //Fin de hilo de lectura

    //Registrar menu
    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        getMenuInflater().inflate(R.menu.menuhome,menu);
        return super.onCreatePanelMenu(featureId, menu);
    }
    //Fin de registro menu

    //Validar opciones de menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(!blockback){
            if(item.getItemId() == R.id.menuabout){
                alert();
            }
            if(item.getItemId() == R.id.menusettings){
                Intent s = new Intent(HomeActivity.this, Settings.class);
                startActivity(s);
            }
        }else
            Toast.makeText(this,"Grabando datos por favor espere...",Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }
    //Fin de validar opciones de menu

    //Mensaje emergente de Acerca de
    public void alert(){
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setTitle("Acerca de");
        builder.setMessage("Desarrollado por:\n\t Andres López" +
                "\n\t Israel Ubarnes");
        builder.setNeutralButton("Aceptar",null);
        builder.create();
        builder.show();
    }
    //Fin de mensaje

    //Capturar boton back
    @Override
    public void onBackPressed() {
        if(!blockback)
            super.onBackPressed();
        else
            Toast.makeText(this,"Grabando datos por favor espere...",Toast.LENGTH_SHORT).show();
    }

    //Capturar evento de estado de resumen de la aplicacion
    @Override
    protected void onResume() {
        sensorManager.registerListener(this, sensorAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        datosx = new ArrayList<>();
        datosy = new ArrayList<>();
        datosz = new ArrayList<>();
       /* List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
        if (sensors.size() > 0) {
            sensorManager.registerListener(this, sensors.get(0), SensorManager.SENSOR_DELAY_GAME);
        }*/
        try{
            sensibility = Float.parseFloat(preferences.getString("edtsettings_sensibility","1E-7f"));
            refresh_time = Float.parseFloat(preferences.getString("edtsettings_refreshtime","0.5E+9f"));
            Log.v("Brandon-lp","sensibilidad ->"+sensibility+" Periodo ->"+refresh_time);

        }catch (NumberFormatException e){
            Intent i = new Intent().setClass(this, Settings.class);
            startActivity(i);
        }
        super.onResume();
    }

    //Capturar evento de estado de stop de la aplicacion
    @Override
    protected void onStop() {
        sensorManager.unregisterListener(this);
        super.onStop();
    }

    //Lectura del acelerometro, las unidades de medida son en acelaracion gravitacional  m/s2
    @Override
    public void onSensorChanged(SensorEvent event) {
        synchronized (this){

            long current_time = event.timestamp;

            curX = myRound(event.values[0]);
            curY = myRound(event.values[1]);
            curZ = myRound(event.values[2]);

            String TAG = "Pajimetro";


            /*if (prevX == 0 && prevY == 0 && prevZ == 0){
                last_upadte = current_time;
                last_movement = current_time;
                prevX = curX;
                prevY = curY;
                prevZ = curZ;
                //datosx.add(curX);
                //datosy.add(curY);
                //datosz.add(curZ); //Los valores de Z dependen de la orientacion magnetica del equipo, es decir se necesita giroscopo para leer los valores

            }
            long time_diff = current_time -last_upadte;
            if(time_diff>0){

                float movement = Math.abs((curX+curY+curZ)-(prevX+prevY+prevZ))/time_diff;  //Calculo del diferencial de acelaracion en todos los ejes.

                //float refresh_time = 0.5E+9f;
                //float min_movement = 1E-7f;

                if (movement > sensibility){*/

            //if (current_time - last_movement >=refresh_time/*400000000*/){
                        /*//Log.v("Brandon-lp","Sensibilidad cumplida "+ (current_time - last_movement)+ "> "+ refresh_time);
                        //datosx.add(curX);
                        //datosy.add(curY);
                        //datosz.add(curZ);
                        Log.v("Brandon-lp","Insersion");


                    }
                    last_movement = current_time;
                }
                    prevX = curX;
                    prevY = curY;
                    prevZ = curZ;

                    last_upadte = current_time;

            }*/


            txtejex.setText("X=> "+ String.format("%.1f", curX));
            txtejey.setText("Y=> "+ String.format("%.1f", curY));
            txtejez.setText("Z=> "+ String.format("%.1f", curZ));
        }
    }

    //Este metodo no es utilizado pero es necesario sobre escribirlo
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}