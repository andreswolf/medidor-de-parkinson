package com.lp.brand.iotacelerometer;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.ArrayList;
import java.util.Calendar;
public class MainActivity extends AppCompatActivity {

    ArrayList<Entry> entradasX, entradasY, entradasZ, entradasXYZ, entradasXYZ2;
    ArrayList<String> labelX, labelY, labelZ, labelXYZ, labelXYZ2;
    LineChart lineChartX,lineChartY,lineChartZ, lineChartXYZ, lineChartXYZ2, lineChartXYZ3;

    TextView unidades;

    String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        entradasX = new ArrayList<>();
        entradasY = new ArrayList<>();
        entradasZ = new ArrayList<>();
        entradasXYZ = new ArrayList<>();
        entradasXYZ2 = new ArrayList<>();
        labelX = new ArrayList<>();
        labelY = new ArrayList<>();
        labelZ = new ArrayList<>();
        labelXYZ = new ArrayList<>();
        labelXYZ2 = new ArrayList<>();
        llenardatos(entradasX, (ArrayList<Float>) getIntent().getSerializableExtra("datosx"),labelX);
        llenardatos(entradasY, (ArrayList<Float>) getIntent().getSerializableExtra("datosy"),labelY);
        llenardatos(entradasZ, (ArrayList<Float>) getIntent().getSerializableExtra("datosz"),labelZ);
        llenardatos(entradasXYZ, (ArrayList<Float>) getIntent().getSerializableExtra("datosxyz"),labelXYZ);
        llenardatos(entradasXYZ2, (ArrayList<Float>) getIntent().getSerializableExtra("datosxyz2"),labelXYZ2);

        unidades = (TextView) findViewById(R.id.text_unidades);

        loadgraphs();
        setUnidades();
    }

    private void setUnidades() {
        unidades.setText("" + String.format("%.2f", calculateTotalArea()) + " Hertz/S");
    }


    public double calculateTotalArea(){
        ArrayList<Float> ys = (ArrayList<Float>) getIntent().getSerializableExtra("datosxyz2");
        if(ys == null){
            ys = new ArrayList<>();
        }
        ArrayList<Point> points = new ArrayList<>();
        for (int i = 0; i < ys.size(); i++) {
            points.add(new Point((float)i, ys.get(i)));
        }

        double area = 0;
        for (int i = 0; i < ys.size() - 1; i++) {
            Point a = points.get(i);
            Point b = points.get(i+1);
            Point c = a.otherPoint(b);
            area += (a.getDistance(c) * b.getY())/2;
        }


        try{
            Point a = points.get(points.size() - 1);
            Point b = points.get(points.size() - 2);
            Point c = a.otherPoint(b);
            double d = c.getDistance(new Point((points.size()-1), 0));
            area += (d * b.getY())/2;

        }catch (Exception e){e.printStackTrace();}

        return area/points.size();

    }




    //Cargar Graficas en los 3 ejes.
    public void loadgraphs(){
        // Todos los ejes
        lineChartXYZ = (LineChart) findViewById(R.id.linechartXYZ);
        lineChartXYZ.animateY(5000);
        ArrayList<Entry> dataSetX = new ArrayList<>();
        ArrayList<Float> arrayX = (ArrayList<Float>) getIntent().getSerializableExtra("datosx");
        if(arrayX == null){
            arrayX = new ArrayList<Float>();
        }
        String[] xAxis = new String[arrayX.size()];
        for (int i = 0; i < arrayX.size(); i++) {
            xAxis[i] =  i + "";
            dataSetX.add(new Entry(arrayX.get(i), i));
        }

        ArrayList<Entry> dataSetY = new ArrayList<>();
        ArrayList<Float> arrayY = (ArrayList<Float>) getIntent().getSerializableExtra("datosy");
        if(arrayY == null){
            arrayY = new ArrayList<Float>();
        }
        for (int i = 0; i < arrayX.size(); i++) {
            dataSetY.add(new Entry(arrayY.get(i), i));
        }


        ArrayList<Entry> dataSetZ = new ArrayList<>();
        ArrayList<Float> arrayZ = (ArrayList<Float>) getIntent().getSerializableExtra("datosz");
        if(arrayZ == null){
            arrayZ = new ArrayList<Float>();
        }
        for (int i = 0; i < arrayX.size(); i++) {
            dataSetZ.add(new Entry(arrayZ.get(i), i));
        }

        Log.i(TAG, "loadgraphs: " + calculateTotalArea());

        ArrayList<ILineDataSet> lines = new ArrayList<ILineDataSet> ();
        LineDataSet lDataSetX = new LineDataSet(dataSetX, "Values X");
        lDataSetX.setColor(Color.RED);
        lDataSetX.setCircleColor(Color.RED);
        LineDataSet lDataSetY = new LineDataSet(dataSetY, "Values Y");
        lDataSetY.setColor(Color.BLUE);
        lDataSetY.setCircleColor(Color.BLUE);
        LineDataSet lDataSetZ = new LineDataSet(dataSetZ, "Values Z");
        lDataSetZ.setColor(Color.GREEN);
        lDataSetZ.setCircleColor(Color.GREEN);

        lines.add(lDataSetX);
        lines.add(lDataSetY);
        lines.add(lDataSetZ);

        lineChartXYZ.setData(new LineData(xAxis, lines));

        ArrayList<Float> arrayXYZ = (ArrayList<Float>) getIntent().getSerializableExtra("datosxyz");
        if(arrayXYZ == null){
            arrayXYZ = new ArrayList<Float>();
        }
        ArrayList<Float> arrayXYZ2 = (ArrayList<Float>) getIntent().getSerializableExtra("datosxyz2");
        if(arrayXYZ2 == null){
            arrayXYZ2 = new ArrayList<Float>();
        }

        for (int i = 0; i < arrayXYZ.size(); i++) {
            Log.i(TAG, "loadgraphs arrayXYZ: " + arrayXYZ.get(i));
            Log.i(TAG, "loadgraphs arrayXYZ2: " + arrayXYZ2.get(i));
        }

        lineChartXYZ2 = (LineChart) findViewById(R.id.linechartXYZ2);
        lineChartXYZ2.animateY(5000);
        LineDataSet lineDataSetXYZ2 = new LineDataSet(entradasXYZ,"Movimientos");
        lineDataSetXYZ2.setColor(Color.BLUE);
        lineDataSetXYZ2.setCircleColor(Color.BLUE);
        LineData lineDataXYZ2 = new LineData(labelXYZ, lineDataSetXYZ2);
        lineChartXYZ2.setData(lineDataXYZ2);
        lineChartXYZ2.setDescription("Movimientos en el tiempo");

        lineChartXYZ3 = (LineChart) findViewById(R.id.linechartXYZ3);
        lineChartXYZ3.animateY(5000);
        LineDataSet lineDataSetXYZ3 = new LineDataSet(entradasXYZ2,"Movimientos");
        lineDataSetXYZ3.setColor(Color.BLUE);
        lineDataSetXYZ3.setCircleColor(Color.BLUE);
        LineData lineDataXYZ3 = new LineData(labelXYZ, lineDataSetXYZ3);
        lineChartXYZ3.setData(lineDataXYZ3);
        lineChartXYZ3.setDescription("Movimientos en el tiempo");

        //ejeX
        lineChartX = (LineChart) findViewById(R.id.linechartX);
        lineChartX.animateY(5000);
        //lineChart.animateX(5000, Easing.EasingOption.Linear);
        LineDataSet lineDataSet = new LineDataSet(entradasX,"Movimientos en el eje x");
        lineDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFillColor(ColorTemplate.COLORFUL_COLORS[0]);
        LineData lineData= new LineData(labelX, lineDataSet);
        lineChartX.setData(lineData);
        lineChartX.setDescription("Movimientos en el tiempo");

        //ejeY
        lineChartY = (LineChart) findViewById(R.id.linechartY);
        lineChartY.animateY(5000);
        //lineChart.animateX(5000, Easing.EasingOption.Linear);
        LineDataSet lineDataSetY = new LineDataSet(entradasY,"Movimientos en el eje Y");
        lineDataSetY.setColors(ColorTemplate.JOYFUL_COLORS);
        lineDataSetY.setDrawFilled(true);
        lineDataSetY.setFillColor(ColorTemplate.COLORFUL_COLORS[1]);
        LineData lineDataY= new LineData(labelY, lineDataSetY);
        lineChartY.setData(lineDataY);
        lineChartY.setDescription("Movimientos en el tiempo");
        //ejeZ
        lineChartZ = (LineChart) findViewById(R.id.linechartZ);
        lineChartZ.animateY(5000);
        //lineChart.animateX(5000, Easing.EasingOption.Linear);
        LineDataSet lineDataSetZ = new LineDataSet(entradasZ,"Movimientos en el eje Z");
        lineDataSetZ.setColors(ColorTemplate.LIBERTY_COLORS);
        lineDataSetZ.setDrawFilled(true);
        lineDataSetZ.setFillColor(ColorTemplate.COLORFUL_COLORS[2]);
        LineData lineDataZ= new LineData(labelZ, lineDataSetZ);
        lineChartZ.setData(lineDataZ);
        lineChartZ.setDescription("Movimientos en el tiempo");
        //Log.v("Brandon-lp2", String.valueOf(lineChartX.saveToGallery("-EjeX",80)));

    }

    private boolean safegraph() {
        Log.v("Brandon-lp","Si entra");
        Calendar calendar = Calendar.getInstance();
        String path=calendar.get(Calendar.DATE)+"/"+(calendar.get(Calendar.MONTH)+1)+"/"+calendar.get(Calendar.YEAR);
        //Log.v("Brandon-lp", "Eje x "+String.valueOf(lineChartX.saveToGallery(path+"-EjeX",80)));
        //Log.v("Brandon-lp", "Eje y "+String.valueOf(lineChartY.saveToGallery(path+"-EjeY",80)));
        //Log.v("Brandon-lp", "Eje z "+String.valueOf(lineChartZ.saveToGallery(path+"-EjeZ",80)));
        return  lineChartX.saveToGallery(path+"-EjeX",80)&&lineChartY.saveToGallery(path+"-EjeY",80)&&lineChartZ.saveToGallery(path+"-EjeZ",80);

    }

    //Convertir datos (parsearizacion)
    public void llenardatos(ArrayList<Entry> entradas, ArrayList<Float> data, ArrayList<String> label){
        if(data == null){
            return;
        }
        for (int i=0; i<data.size(); i++){
            entradas.add(new BarEntry(data.get(i),i));
            label.add("P"+i);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //registro de menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menumain,menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Validacion de opciones de menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        if(item.getItemId() == R.id.menurestart){
            i = new Intent().setClass(MainActivity.this,HomeActivity.class);
            startActivity(i);
            finish();
        }
        if(item.getItemId() == R.id.menusendmail){
            i= new Intent(MainActivity.this,EnviarActivity.class);
            startActivity(i);
        }
        if(item.getItemId() == R.id.menusafegraph){
            if (safegraph()) Toast.makeText(this,"Guardado Correctamente",Toast.LENGTH_SHORT).show();
            else Toast.makeText(this,"Error al guardar, se necesita una SDcard",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }



    //Capturar boton back
    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Para regresar utilice el menu",Toast.LENGTH_SHORT).show();
    }

    private class Point{
        public float x, y;

        public Point(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public double getY(){
            return (double) Math.abs(this.y);
        }

        public double getDistance(Point p){
            return Math.sqrt(((p.x - this.x)*(p.x - this.x)) + ((p.y - this.y)*(p.y - this.y)));
        }

        public float evaluatePoint0(Point p){
            String fx = getFuntionByPoint(x, y, p.x, p.y);
            return (float)evaluateFuntion(fx, 0);
        }

        private String getFuntionByPoint(float x1, float y1, float x2, float y2){
            float a = (y1-y2)/(x1-x2);
            float b = y1-(x1*a);
            String fx = a + "*x+" + b;
            return fx;
        }

        private double evaluateFuntion(String funtion, float x){
            String VARIABLE = "x";
            Expression e = new ExpressionBuilder(funtion)
                    .variable(VARIABLE)
                    .build()
                    .setVariable(VARIABLE, x);

            return e.evaluate();
        }

        public Point otherPoint(Point p){
            return new Point(evaluatePoint0(p),0.0f);
        }


    }

}
