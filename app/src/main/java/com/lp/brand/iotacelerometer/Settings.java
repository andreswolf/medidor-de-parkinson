package com.lp.brand.iotacelerometer;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by brand on 27/05/2016.
 */
public class Settings extends PreferenceActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}
