package com.lp.brand.iotacelerometer;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.lp.brand.iotacelerometer.databinding.ActivityEnviarBinding;

public class EnviarActivity extends AppCompatActivity {
    EditText edtemail, edtasunto;
    Button btnenviar;
    ProgressBar progressBar;
    Handler handler;
    int progressStatus=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar);
        edtemail =(EditText) findViewById(R.id.edtEmail);
        edtasunto =(EditText) findViewById(R.id.edtasunto);
        btnenviar = (Button) findViewById(R.id.btnsendmail);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        handler = new Handler();
        btnenviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar())
                    progressBar.setVisibility(View.VISIBLE);
                fakeload();
            }
        });

    }

    private boolean validar() {
        if(TextUtils.isEmpty(edtemail.getText().toString())){
            edtemail.setError("Digite una direccion de correo!");
            return false;
        }
        if(TextUtils.isEmpty(edtasunto.getText().toString())){
            edtasunto.setError("Campo requerido!");
            return false;
        }
        return true;
    }


    private void fakeload() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressStatus < 100){
                    //actualizar progressbar
                    progressStatus++;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progressStatus);
                            if(progressStatus==100){
                                alert();//Log.v("Brandon-lp","finalizado");
                                progressBar.setVisibility(View.INVISIBLE); }
                        }
                    });

                    //simular proceso
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();

    }

    private void alert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exito!");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Regresar a las estadisticas");
        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(EnviarActivity.this, MainActivity.class);
                startActivity(i);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                edtasunto.setText("");
                edtemail.setText("");
                progressBar.setProgress(0);
            }
        });
        builder.show();
    }


}
